package com.upiita.translator.controller;

import javax.validation.Valid;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.upiita.translator.model.number;

@RestController
@RequestMapping("/translator")
public class translatorController {
	
		
		@GetMapping
		public String get() {
			return "Use Metod Post";
		}
		
		@PostMapping(consumes= {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
		public ResponseEntity<number>translate(@Valid @RequestBody  number number){
			number.translation();
			return new ResponseEntity<number>(number,HttpStatus.OK);
		}
		
		@PutMapping
		public String update() {
			return "Use Metod Post";
		}
		
		@DeleteMapping
		public String delete() {
			return "Use Metod Post";
		}

	}
