package com.upiita.translator.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class number {
	private int number;
	@NotEmpty(message= "The field cannot be empty.")
	@Size(min = 1, max = 10, message = "The number isn't in the range permitted.")
	private String translation;
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getTranslation() {
		return translation;
	}
	public void setTranslation(String translation) {
		this.translation = translation;
	}
	
	public  void translation() {
		String[] number = {"n’a","yoho", "hñu", "goho", "küt’a", "r’ato", "yoto", "hñäto", "güto", "r’ët’a"};
		this.translation = number[this.number-1	];
	}

}
