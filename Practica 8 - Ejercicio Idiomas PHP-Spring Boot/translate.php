<?php

	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	$data = json_decode(file_get_contents("php://input"));
	$met = $_SERVER['REQUEST_METHOD'];
	$uri = $_SERVER['REQUEST_URI'];
	$numOtomi = array('n’a','yoho', 'hñu', 'goho', 'küt’a', 'r’ato', 'yoto', 'hñäto', 'güto', 'r’ët’a');
	if($met == 'POST'){
		if(!empty($data->num)){
			if ($data->num > 0 && $data->num < 11){
				$message="Successful translation";
				echo json_encode(array(
					"value" => $data->num,
					"pronunciation" => $numOtomi[$data->num-1]
					)
				);
			}
			else{
				http_response_code(503);
				echo json_encode(array("message" => "Data received not supported"));
			}
		}
		else{
			http_response_code(503);
			echo json_encode(array("message" => "No data was received"));
		}
	}
?>
