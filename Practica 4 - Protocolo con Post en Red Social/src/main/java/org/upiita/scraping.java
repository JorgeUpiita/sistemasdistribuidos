package org.upiita;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class scraping {
    static List<Pizza> listPizzaInfo, listPizzaSearch;
    static List<WebElement> listPizzaRecopiled;
    static Pizza element;
    static WebDriver driver;
    static String chromePath = System.getProperty("user.dir") + "\\drivers\\chromedriver.exe";

    public static String findPizza(String product){
        String result = "";
        float price = 10000;
        String sprice;
        Pizza select = new Pizza();
        listPizzaInfo =  new ArrayList<Pizza>();
        listPizzaSearch =  new ArrayList<Pizza>();
        listPizzaRecopiled = new ArrayList<WebElement>();
        String baseURL;
        String xPath;
        System.setProperty("webdriver.chrome.driver", chromePath);
        driver = new ChromeDriver();
        //Little Caesar
        baseURL= "https://www.ubereats.com/mx/mexico-city/food-delivery/little-caesars-montevideo/G87YejRrQrqWO49hww5fHQ";
        xPath = "//*[@id=\"wrapper\"]/main/div[2]/ul/li[4]/ul/li";
        scrapingPizza(baseURL,xPath,"Little Caesars");

        //Pizza Hut
        baseURL= "https://www.ubereats.com/mx/mexico-city/food-delivery/pizza-hut-plaza-torres-lindavista/ApRlA4hTSuKYsDEHGAE00A";
        xPath ="//*[@id=\"wrapper\"]/main/div[2]/ul/li[4]/ul/li";
        scrapingPizza(baseURL,xPath,"Pizza Hut");

        //Papa John's
        baseURL= "https://www.ubereats.com/mx/mexico-city/food-delivery/papa-johns-pizza-lindavista/miEwW3wOR-iM3IFrvlRLTQ";
        xPath = "//*[@id=\"wrapper\"]/main/div[2]/ul/li[8]/ul/li";
        scrapingPizza(baseURL,xPath,"Papa John's");

        driver.quit();
        try {

            for (Pizza p : listPizzaInfo) {
                if (p.type.toLowerCase().contains(product) || p.description.toLowerCase().contains(product)) {
                    listPizzaSearch.add(p);
                }
            }
            for (Pizza p : listPizzaSearch) {
                sprice = p.price.substring(1, p.price.length());
                if(scraping.isNumeric(sprice)) {
                    if (price > Float.valueOf(sprice)) {
                        select = p;
                        price = Float.valueOf(sprice);
                    }
                }
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        result = "#Marca: " + select.brand + " #Tipo: "+ select.type + " #Precio: " + select.price;
        post.post("La mejor pizza para ti:\n" + result+"\nEncuentrala en: " + select.baseURL);
        return result = "#Marca: " + select.brand + " #Tipo: " + select.type + " #Descripción: " + select.description + " #Precio: " + select.price;
    }

    public static void scrapingPizza(String baseURL, String xPath, String brand){
        try {
            driver.get(baseURL);
            listPizzaRecopiled = driver.findElements(By.xpath(xPath));
            for (WebElement dataPizza: listPizzaRecopiled) {
                element = new Pizza(brand, dataPizza, baseURL);
                listPizzaInfo.add(element);
            }
        }catch (NoSuchElementException ne){
            System.err.println("No se encontró el Web Element: " + ne.getMessage());
        }catch (Exception e){
            System.err.println("Error de java " + e.getMessage());
        }

    }

    private static boolean isNumeric(String cadena){
        try {
            Float.parseFloat(cadena);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }
}
