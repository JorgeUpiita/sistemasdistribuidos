const mongoose = require("./mongo-init");
var express = require("express");
var app = express();

const Schema = mongoose.Schema;
const ANY = new Schema(
  {
    texto: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
  },
  { strict: false, versionKey: false }
);
const saludoModel = mongoose.model("saludos", ANY);
app.get("/", function (req, res) {
  res.send("Estás en el root");
});

app.get("/saludo3desc", async function (req, res) {
  let r = await saludoModel
    .find({})
    .limit(3)
    .sort({ createdAt: "desc" })
    .exec();
  res.json(r);
});

app.get("/saludo3asc", async function (req, res) {
  let r = await saludoModel
    .find({})
    .limit(3)
    .sort({ createdAt: "asc" })
    .exec();
  res.json(r);
});


app.get("/saludos", async function (req, res) {
  let r = await saludoModel.find({}).sort({ createdAt: "desc" }).exec();
  res.json(r);
});

app.get("/saludos/eliminar/todos", async function (req, res) {
  saludoModel.remove({}, function (err) {
    res.send("Hecho");
  });
});
app.get("/saludo/crear", async function (req, res) {
  let r = await saludoModel.create({ texto: req.query.texto });
  res.json(r.toObject());
});

app.get("/saludo/crear_c", async function (req, res) {
  
  NC=req.query.texto.length;
  Saludo=req.query.texto+"-"+NC
  let r = await saludoModel.create({ texto: Saludo });
  res.json(r.toObject());
});


app.listen(3000, function () {
  console.log("Servidor listo en el puerto 3000");
});
